﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;


public class QuitMenu : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
		GetComponent<Button>().onClick.AddListener(() => EditorPlaying());
    }

    void EditorPlaying()
    {
		EditorApplication.Exit(1);
    }
}
