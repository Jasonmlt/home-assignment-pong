﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ball : MonoBehaviour {

    float ballSpeed=20f;

    public Text ScoreText1;
    public Text ScoreText2;

    private Rigidbody2D rb;


    int Score1 = 0;
    int Score2 = 0;
	public static int saveScore1 = 0; //static: to retain total score
	public static int saveScore2 = 0;

    bool isPlay;
    int randInt;

    // Use this for initialization
    void start()
    {
        rb = GameObject.FindWithTag("B").GetComponent<Rigidbody2D>();
        
    }


    void Awake () {
        rb = gameObject.GetComponent<Rigidbody2D>();
        randInt = Random.Range(1, 3);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0) == true && isPlay == false)
        {

            isPlay = true;
            rb.isKinematic = false;
            if (randInt == 1)
            {
                rb.velocity = new Vector2(ballSpeed, ballSpeed);
            }
            if (randInt == 2) 
            {
                rb.velocity = new Vector2(-ballSpeed, -ballSpeed);
            }
        }

        ScoreText1.text = " " + Score1;
        ScoreText2.text = " " + Score2;


        //if current scene is main load lvl2

        //{
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        if (sceneName == "Main")
        {
            if (Score1 == 2 || Score2 == 2)
            {
                SceneManager.LoadScene("Mainlvl2");
                saveScore1 += Score1;
                saveScore2 += Score2;
                print(saveScore1 + " " + saveScore2);
                
                //Score1 = 0;
                //Score2 = 0;

            }
        }

        if (sceneName == "Mainlvl2")
        {
            if (Score1 == 5 || Score2 == 5)
            {
                SceneManager.LoadScene("Mainlvl3");
                saveScore1 += Score1;
                saveScore2 += Score2 ;
                print(saveScore1 + " " + saveScore2);
 
                //Score1 = 0;
                //Score2 = 0;
            }
        }


        if (sceneName == "Mainlvl3")
        {
            if (Score1 == 7 || Score2 == 7)
            {
                SceneManager.LoadScene("End Menu");
                saveScore1 += Score1;
                saveScore2 += Score2;
                print(saveScore1 + " " + saveScore2);

                
                //Score1 = 0;
                //Score2 = 0;
            }
        }

        //if current scene is main load lvl3 


    }


    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag== "leftGoal")
        {
            Score2 += 1;
            StartCoroutine(playerWait(1));
        }
        if (target.tag == "rightGoal")
        {
            Score1 += 1;
            StartCoroutine(playerWait(2));

        }

    }

    IEnumerator playerWait(int playerGoal)
    {
        transform.position = new Vector2(0, 0);
        rb.bodyType = RigidbodyType2D.Static;
        yield return new WaitForSeconds(3);
        rb.bodyType = RigidbodyType2D.Dynamic;    

        if (playerGoal == 1)
        {
            rb.velocity = new Vector2(20f, -20f);
        }
        else { 
            rb.velocity = new Vector2(-20f, 20f);
        }
    }

}

