﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    public float paddleVelocity = 1;
    public Vector2 playerPosition= new Vector2(0,0);


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float yPos = gameObject.transform.position.y + (Input.GetAxis("Vertical") * paddleVelocity);
        playerPosition = new Vector2(-23, Mathf.Clamp(yPos, -11, 11));
        gameObject.transform.position = playerPosition;


    }
}
