﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
	
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => loadMain());
    }

    public void loadMain()
    {
        SceneManager.LoadScene("Main");
		ball.saveScore1 = 0;
		ball.saveScore2 = 0;

    }

}