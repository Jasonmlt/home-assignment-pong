﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class displayScore : MonoBehaviour {

	// Use this for initialization
	public Text playerScore1;
	public Text playerScore2;
	public Text announce;

	void Start () {
		playerScore1.text = ball.saveScore1+" ";
		playerScore2.text = ball.saveScore2+" ";

		if (ball.saveScore1 > ball.saveScore2) {
			announce.text = "Congrats Player 1";

		}else if (ball.saveScore1 < ball.saveScore2) {
			announce.text = "Congrats Player 2";

		}else {
			announce.text = "It's a Draw!!";
		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
