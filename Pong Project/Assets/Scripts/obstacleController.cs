﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleController : MonoBehaviour {

    bool up = true;
    Vector2 startPos;

    void Start () {
        startPos = transform.position;

    }

    //// Update is called once per frame
    void Update () {
        if (startPos.y >= 9.7) {
            up = false;
        }else if (startPos.y<=-9.7)
        {
            up = true;
        }
        if (up) {
            startPos.y+=.6f;
        }else
        {
            startPos.y-=.6f;
        }
        transform.position = startPos;
        
    }

}
