﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController2 : MonoBehaviour
{


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        Vector2 mousePosition = Input.mousePosition;
        Vector2 paddlePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = new Vector2(23,Mathf.Clamp(paddlePosition.y, -11f, 11f));

    }
}

